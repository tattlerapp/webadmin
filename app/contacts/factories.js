(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.factory('ContactsRepository', [ '$resource', '$cookies', 'AppConfig', function ($resource, $cookies, AppConfig) {

        var url = AppConfig.server + "company/contacts";

        return $resource(url, {}, {
            all: {method: 'GET', isArray: true}
        });

    }]);

})();