(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.controller("ContactsController", ["$scope", "ContactsRepository", function ($scope, ContactsRepository) {

        /*
        //TODO: Remove commented out after demo
        var jsonContacts = [
            {
                "id": "1",
                "name": "John",
                "last_name": "Doe",
                "profile_image": "https://farm7.staticflickr.com/6163/6195546981_200e87ddaf_b.jpg"
            },
            {
                "id": "2",
                "name": "Juan",
                "last_name": "Perez",
                "profile_image": "https://farm7.staticflickr.com/6163/6195546981_200e87ddaf_b.jpg"
            }
        ];

        $scope.contacts = angular.fromJson(jsonContacts);
        */

        ContactsRepository.all({}, function (contacts) {
            $scope.contacts = contacts;
        });

    }]);

    app.config(function ($routeProvider) {
        $routeProvider
            .when("/contacts", {
                templateUrl: "contacts/partials/index.html",
                css: "contacts/contacts.css",
                controller: "ContactsController"
            });
    });

})();