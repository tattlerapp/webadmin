(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.factory('ConektaService', function ($rootScope) {

        Conekta.setPublishableKey("key_J9urXUoXhPuv5Qty6WskttA");

        var service = {};

        service.createToken = function (card) {
            Conekta.token.create({card: card}, function (response) {
                $rootScope.$apply(function () {
                    service.successCallback(response.id);
                });
            }, function (response) {
                $rootScope.$apply(function () {
                    service.errorCallback(getMessage(response));
                });
            });

        };

        var getMessage = function (response) {

            var message;

            switch (response.code) {
                case 'invalid_number':
                    message = "El número de la tarjeta es inválido.";
                    break;
                case 'invalid_expiry_month':
                    message = "El mes de expiración de la tarjeta es inválido.";
                    break;
                case 'invalid_expiry_year':
                    message = "El año de expiración de la tarjeta es inválido.";
                    break;
                case 'invalid_cvc':
                    message = "El código de seguridad de la tarjeta es inválido.";
                    break;
                case 'expired_card':
                    message = "La tarjeta ha expirado.";
                    break;
                case 'processing_error':
                    message = "Ha habido un error al momento de procesar la tarjeta. Ningún cargo ha sido realizado.";
                    break;
                case 'insufficient_funds':
                    message = "El cargo no ha sido procesado porque la tarjeta no tiene fondos suficientes.";
                    break;
                default :
                    message = "Hubo un error al procesar la tarjeta.";
                    break;
            }

            return message;
        };

        return {
            getCardToken: function (card, successCallback, errorCallback) {
                service.successCallback = successCallback;
                service.errorCallback = errorCallback;
                service.createToken(card);
            }
        }

    });

})();