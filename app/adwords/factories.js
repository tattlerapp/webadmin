(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.factory('AdwordsRepository', [ '$resource', '$cookies', 'AppConfig', function ($resource, $cookies, AppConfig) {

        var url = AppConfig.server + "adwords";

        return $resource(url, {}, {
            create: {method: 'POST'},
            acquired: {url: AppConfig.server + 'adwords/acquired', method: 'GET', isArray: false}
        });

    }]);

    app.factory('PlansRepository', [ '$resource', '$cookies', 'AppConfig', function ($resource, $cookies, AppConfig) {

        var url = AppConfig.server + "plans";

        return $resource(url, {}, {
            all: {method: 'GET', isArray: true}
        });

    }]);

})();