(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.controller("AdwordsController", ["$scope", "AdwordsRepository", function ($scope, AdwordRepository) {

    }]);

    app.controller("NewAdwordsController", function ($scope, $window, SweetAlert, AdwordsRepository, PlansRepository, ConektaService) {

        AdwordsRepository.acquired({},
            function (adword) {
                SweetAlert.swal({
                        title: "Ya cuentas con un paquete Tattler Adwords contratado", text: "", type: "error",
                        showCancelButton: false, confirmButtonColor: "#428bca", confirmButtonText: "Ir a promociones"
                    },
                    function () {
                        $window.location.href = '#/promotions';
                    }
                );
            });


        PlansRepository.all({}, function(plans){
            $scope.plans = plans;
            $scope.selectedPlan = $scope.plans[0];
        });

        $scope.step = 1;
        $scope.card = {};
        $scope.showError = false;

        $scope.$watch('selectedPlan', function (plan) {
            if (plan == undefined) return;
            $scope.selectedIncome = plan.mean_incomes[0];
        });

        $scope.acquireAdword = function(){

            var adword = {
                token_id : $scope.conektaToken,
                plan_id: $scope.selectedPlan.id
            };

            AdwordsRepository.create({}, adword, function (response) {

                SweetAlert.swal({
                        title: "Tattler Adwords contratado!", text: "", type: "success",
                        showCancelButton: false, confirmButtonColor: "#428bca", confirmButtonText: "Ir a promociones"
                    },
                    function () {
                        $window.location.href = '#/promotions';
                    }
                );

            },function(error){
                SweetAlert.swal(error.data.Message, "", "error");
            });
        };

        $scope.setStep = function (step) {
            $scope.step = step;
            $scope.showError = false;
        };

        $scope.isStep = function (step) {
            return $scope.step == step;
        };

        $scope.setPayData = function () {
            ConektaService.getCardToken($scope.card, function (token) {
                var number = $scope.card.number.toString();
                $scope.last4 = "****" + number.substr(number.length - 4);
                $scope.conektaToken = token;
                $scope.setStep(3);
            }, function (message) {
                $scope.errorMessage = message;
                $scope.showError = true;
            });
        };

    });

    app.config(function ($routeProvider) {
        $routeProvider
            .when("/adwords/new", {
                templateUrl: "adwords/partials/new.html",
                css: "adwords/contacts.css",
                controller: "NewAdwordsController"
            });
    });

})();