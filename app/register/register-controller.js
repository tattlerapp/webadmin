(function() {

    'use strict';

    var app = angular.module("tattlerApp");

    app.controller("RegisterController",
        function ($scope, $http, AppConfig) {
            $scope.places=null;
            $scope.originalUser = angular.copy($scope.user);
            var route = AppConfig.server + "companies";
            var routeCategories = AppConfig.server + "categories";
            $http({
                method: "get",
                url: routeCategories
            })
            .success(function (data) {
                $scope.categories=[];
                angular.forEach(data, function(value, key) {
                    $scope.categories.push(
                    {
                        icon:"<img src="+value.image+" />",
                        name:value.name,
                        maker:value.id,
                        ticked: false
                    });
                });
            });

            $scope.resetUserForm = function (isOk) {
                $scope.successfull = isOk;
                $scope.showMessage = true;
                if(!isOk){
                    $scope.user = angular.copy($scope.originalUser);
                    $scope.registerForm.$setPristine();
                }
                $scope.submitProgress = false;
                $scope.showValidation = false;
            };

            $scope.RegisterUser = function (userDetails) {
                if (!$scope.registerForm.$invalid && $scope.resultCategory.length>0) {
                    $scope.submitProgress = true;
                    $http.post(route, {
                        "administrator": {
                            "email": userDetails.email,
                            "password": userDetails.password
                        },
                        "name": userDetails.name + " " + userDetails.lastName,
                        "rfc": userDetails.rfc,
                        "category_id": $scope.resultCategory[0].maker,
                        "address": userDetails.address
                    }).
                        success(function (data, status) {
                            $scope.resetUserForm(true);
                        }).
                        error(function (data, status) {
                            $scope.resetUserForm(false);
                        });
                } else {
                    $scope.showValidation = true;
                    if($scope.resultCategory.length==0){
                        $scope.showErrorCategory = true;
                    }
                    else{
                        $scope.showErrorCategory = false;
                    }
                }
            };

            $scope.$watch('resultCategory.length', function (newValue, oldValue) {
                if(newValue>0 && $scope.showErrorCategory == true){
                    $scope.showErrorCategory = false;
                }
            });

            $scope.Options = {
              country: 'mx',
              types: 'geocode',
              watchEnter: false
            };

            $scope.getError = function (error) {
                if (angular.isDefined(error)) {
                    if (error.required) {
                        return "Por favor, introduzca un valor.";
                    } else if (error.email) {
                        return "Por favor, introduzca un Email v\u00e1lido";
                    } else if (error.pattern) {
                        return "Por favor, introduzca un valor v\u00e1lido";
                    } else if (error.minlength) {
                        return "No contiene los caracteres minimos(8 para la contrase\u00F1a y 13 para el RFC)";
                    } else if(error.maxlength){
                        return "Contiene m\u00e1s caracteres de lo debido";
                    } else if (error.passwordVerify) {
                        return "La contrase\u00F1a no coincide";
                    } 
                }
            };

            $scope.autocompleteOptions = {
                componentRestrictions: { country: 'MX' },
                types: ['(cities)']
            }
        });

})();