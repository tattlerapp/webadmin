(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.service("CompanyService", function (SweetAlert, CompanyRepository, CompanyImageRepository) {

        var _current = function (onSucess, onError) {
            CompanyRepository.current({}, onSucess, onError);
        };

        var _updateCompany = function (profileUri, backgroundUri, description, afterUpdate, onUpdateFailure) {

            var data = {
                description: description
            };

            CompanyRepository.update({}, data, function () {
                if (profileUri != undefined) { //Profile Image selected
                    CompanyImageRepository.uploadProfile(profileUri).then(function (response) {
                        if (backgroundUri != undefined) {//Background Image selected
                            CompanyImageRepository.uploadBackground(backgroundUri).then(function (response) {
                                afterUpdate();
                            }, function (error) {
                                onUpdateFailure();
                            });
                        }
                    }, function (error) {
                        onUpdateFailure();
                    });
                } else if (backgroundUri != undefined) {//Background Image selected
                    CompanyImageRepository.uploadBackground(backgroundUri).then(function (response) {
                        afterUpdate();
                    }, function (error) {
                        onUpdateFailure();
                    });
                } else {
                    afterUpdate();
                }
            }, function (error) {
                onUpdateFailure();
            });

        };

        return {
            updateCompany: _updateCompany,
            current: _current
        }

    });


})();