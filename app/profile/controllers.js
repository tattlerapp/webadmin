(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.controller("EditCompanyController", ["$scope", '$upload', '$timeout', 'ngDialog', 'SweetAlert', "CompanyService", "CompanyImageRepository", "FileUtils",
        function ($scope, $upload, $timeout, ngDialog, SweetAlert, CompanyService, CompanyImageRepository, FileUtils) {

            //Constants
            $scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);

            var _generateThumb = function (model, files) {
                FileUtils.generateThumb(model, files);
            };

            var _updateCompany = function () {

                var description = $scope.company.description;
                var profileUri = $scope.company.updated_profile_image;
                var backgroundUri = $scope.company.updated_background_image.dataUrl;

                CompanyService.updateCompany(profileUri, backgroundUri, description, function () {
                    SweetAlert.swal("Empresa actualizada", "", "success");
                }, function () {
                    SweetAlert.swal("No se pudo actualizar empresa", "", "error");
                });

            };

            var _generateCrop = function (model, files) {

                if (files == undefined || files.length == 0)
                    return;

                var dialog = ngDialog.open({
                    template: "picture-modal",
                    className: 'ngdialog-theme-default',
                    controller: "CropPhotoController",
                    data: {files: files},
                    closeByDocument: true,
                    closeByEscape: true
                });

                dialog.closePromise.then(function (data) {
                    var updateImageUri = data.value;
                    if (updateImageUri == undefined)
                        return;
                    $scope.company.updated_profile_image = updateImageUri;
                });

            };

            $scope.updateCompany = _updateCompany;
            $scope.generateThumb = _generateThumb;
            $scope.generateCrop = _generateCrop;

            //Init
            CompanyService.current({}, function (current) {
                $scope.company = current;
                $scope.company.updated_background_image = {};
            }, function (error) {
            });

        }]);

    app.config(function ($routeProvider) {
        $routeProvider
            .when("/company/profile/edit", {
                templateUrl: "profile/partials/edit.html",
                controller: "EditCompanyController"
            });
    });

})();