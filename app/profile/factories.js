(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.factory('CompanyRepository', ['$resource', '$cookies', 'AppConfig', function ($resource, $cookies, AppConfig) {

        var url = AppConfig.server + "company";

        return $resource(url, {}, {
            update: {url: url + "/profile", method: 'PUT'},
            current: {method: 'GET', isArray: false}
        });

    }]);

    app.factory('CompanyImageRepository', function ($upload, $cookies, AppConfig, FileUtils) {

        var url = AppConfig.server + "company/profile";

        return {
            uploadProfile: function (imageUri) {

                var data = FileUtils.dataURItoBlob(imageUri);

                return $upload.upload({
                    url: url + '/image',
                    method: 'POST',
                    file: data.blob,
                    fileName: data.filename
                });

            },
            uploadBackground: function (imageUri) {

                var data = FileUtils.dataURItoBlob(imageUri);

                return $upload.upload({
                    url: url + '/background',
                    method: 'POST',
                    file: data.blob,
                    fileName: data.filename
                });

            }
        };

    });


})();