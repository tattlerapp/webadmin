(function () {

    'use strict';

    var app = angular.module("tattlerApp",
        [
            "ngResource",
            "ngRoute",
            'LocalStorageModule',
            "routeStyles",
            "chieffancypants.loadingBar",
            "ngAnimate",
            "multi-select",
            "ngCookies",
            "ui.grid",
            "ui.grid.edit",
            "ui.grid.cellNav",
            "ui.grid.selection",
            "ngAutocomplete",
            "ngMap",
            "mgcrea.ngStrap",
            "ngImgCrop",
            "ngTable",
            "nvd3ChartDirectives",
            "oitozero.ngSweetAlert",
            "google.places",
            "angularFileUpload",
            "base64",
            "ngDialog",
            "ui.mask",
            'ui.bootstrap.modal', "template/modal/backdrop.html","template/modal/window.html",
            "ui.bootstrap.tabs", "template/tabs/tabset.html", "template/tabs/tab.html"
        ]
    );

    app.config(function ($routeProvider) {
        $routeProvider
            .when("/register", {
                templateUrl: "register/register.html",
                css: "register/register.css",
                controller: "RegisterController"
            })
            .when("/dashboard", {
                templateUrl: "dashboard/dashboard.html",
                css: "dashboard/dashboard.css",
                controller: "DashboardController"
            })
            .otherwise({
                redirectTo: "/login"
            });
    });

    app.config(function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
    });

    app.controller('LayoutCtrl', function ($scope) {

        $scope.toggleSidebar = function () {
            var $ = angular.element;
            if ($(window).width() <= 992) {
                $('.row-offcanvas').toggleClass('active');
                $('.left-side').removeClass('collapse-left');
                $('.right-side').removeClass('strech');
                $('.row-offcanvas').toggleClass('relative');
            } else {
                /* Else, enable content streching*/
                $('.left-side').toggleClass('collapse-left');
                $('.right-side').toggleClass('strech');
            }
        };

    });

    app.run(function ($rootScope, authService) {

        $rootScope.$on('$routeChangeStart', function () {
            authService.checkStatus();
        });

        $rootScope.logout = function () {
            authService.logOut();
        };
    });

    app.provider('AppConfig', function () {
        var config = {
            server: 'http://ec2-54-69-1-179.us-west-2.compute.amazonaws.com/api/',
            clientId: 'tattlerWebApp'
        };
        return {
            set: function (settings) {
                config = settings;
            },
            $get: function () {
                return config;
            }
        };
    });

    app.config(['ngDialogProvider', function (ngDialogProvider) {
        ngDialogProvider.setDefaults({
            className: 'ngdialog-theme-default',
            plain: false,
            showClose: true,
            closeByDocument: false,
            closeByEscape: false
        });
    }]);

    app.config(function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
    });

    app.run(['authService', function (authService) {
        authService.fillAuthData();
    }]);

})();