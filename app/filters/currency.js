(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.filter('mexicanCurrency', function () {
        return function (input) {
            return input ? '$' + (input * 14 * 1.16 * 1.03).toFixed(2) + ' MXN' : '';
        };
    });

})();