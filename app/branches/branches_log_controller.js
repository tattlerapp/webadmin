(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.controller("BranchesLogController", function ($scope, SweetAlert, $modal, BranchesFactory, BranchLogsRepository) {

            //Properties

            //TODO: Remove commented out after demo
            var callsLogJson = {
                "calls": [
                    {
                        "customer": {
                            "id": "1",
                            "name": "John",
                            "last_name": "Doe",
                            "profile_image": "http://wp.production.patheos.com/blogs/thepangeablog/files/2011/12/Kurt-Profile-Image-Square-Brick-1024x1024.jpg"
                        },
                        "duration": "10 min 3 seg",
                        "is_contact": true,
                        "connected_at": "2015-01-20T06:00:00.0000000Z",
                        "status": "unknown"
                    },
                    {
                        "customer": {
                            "id": "2",
                            "name": "Juan",
                            "last_name": "Perez",
                            "profile_image": "http://wp.production.patheos.com/blogs/thepangeablog/files/2011/12/Kurt-Profile-Image-Square-Brick-1024x1024.jpg"
                        },
                        "duration": "10 min 3 seg",
                        "is_contact": false,
                        "connected_at": "2015-01-20T06:00:00.0000000Z",
                        "status": "unknown"
                    }
                ],
                "total_minutes": 1
            };

            var conversationsJson = [
                {
                    "customer": {
                        "id": "1",
                        "name": "John",
                        "last_name": "Doe",
                        "profile_image": "http://wp.production.patheos.com/blogs/thepangeablog/files/2011/12/Kurt-Profile-Image-Square-Brick-1024x1024.jpg"
                    },
                    "conversation_id": "123091823098",
                    "is_contact": true,
                    "last_message_at": "2015-01-20T06:00:00.0000000Z"
                },
                {
                    "customer": {
                        "id": "2",
                        "name": "Juan",
                        "last_name": "Perez",
                        "profile_image": "http://wp.production.patheos.com/blogs/thepangeablog/files/2011/12/Kurt-Profile-Image-Square-Brick-1024x1024.jpg"
                    },
                    "conversation_id": "123091823098",
                    "is_contact": true,
                    "last_message_at": "2015-01-20T06:00:00.0000000Z"
                }
            ];

            $scope.callsLog = angular.fromJson(callsLogJson);
            $scope.conversationsLog = angular.fromJson(conversationsJson);
            $scope.currentPanel = 1;
            $scope.types = [{id: 1, label: "Llamadas a telefonía fija"}, {id: 2, label: "Mensajería Instantánea"}];
            $scope.selectedType = $scope.types[0];

            //Functions
            var _onSelectedBranch = function (branch) {
            };

            var _onSelectedType = function (type) {
                $scope.currentPanel = type.id;
            };

            var _isPanel = function (currentPanel) {
                return $scope.currentPanel == currentPanel;
            };

            var _showConversation = function (conversation) {

                var modalInstance = $modal.open({
                    templateUrl: 'conversation-modal',
                    controller: 'ConversationModalCtrl',
                    size: 'lg'
                });

            };

            //Functions bindings
            $scope.onSelectedBranch = _onSelectedBranch;
            $scope.onSelectedType = _onSelectedType;
            $scope.isPanel = _isPanel;
            $scope.showConversation = _showConversation;

            //Init
            BranchesFactory.all({}, function (branches) {
                $scope.branches = branches;

                if ($scope.branches.length == 0) {
                    return;
                }

                $scope.selectedBranch = $scope.branches[0];

            });

        });

    app.controller('ConversationModalCtrl', function ($scope, ngDialog, SweetAlert, BranchLogsRepository) {

        var conversationJson = [
            {
                "id": "1",
                "body": "Buen día, ¿tienen servicio a domicilio a Las Americas?",
                "sender": "John Doe",
                "receiver": "Sucursal 2",
                "sent_at": "2015-01-20T06:00:00.0000000Z",
                "conversation_id": "1"
            },
            {
                "id": "2",
                "body": "Sí, ¿cuál sería su pedido?",
                "sender": "Sucursal 2",
                "receiver": "John Doe",
                "sent_at": "2015-01-20T06:00:00.0000000Z",
                "conversation_id": "1"
            },
            {
                "id": "3",
                "body": "Una Pizza Grande de dos ingredientes, la de promoción.",
                "sender": "John Doe",
                "receiver": "Sucursal 2",
                "sent_at": "2015-01-20T06:00:00.0000000Z",
                "conversation_id": "1"
            },
            {
                "id": "3",
                "body": "También me gustaría una orden de pan con ajo.",
                "sender": "John Doe",
                "receiver": "Sucursal 2",
                "sent_at": "2015-01-20T06:00:00.0000000Z",
                "conversation_id": "1"
            },
            {
                "id": "5",
                "body": "Claro, ¿me puede proporcionar su número?",
                "sender": "Sucursal 2",
                "receiver": "John Doe",
                "sent_at": "2015-01-20T06:00:00.0000000Z",
                "conversation_id": "1"
            }
        ];

        $scope.conversation = angular.fromJson(conversationJson);

        console.log($scope.conversation);

        $scope.FirstSender = $scope.conversation[0].sender;

    });

})();