(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.factory('BranchesFactory', ['$resource', 'AppConfig', function ($resource, AppConfig) {

        var url = AppConfig.server + "company/branches";
        var urlCountry = AppConfig.server + "countries";

        return $resource(url, { branch_id: '@id'}, {
            create: {url: url,method: 'POST'},
            all: {method: 'GET', isArray: true},
            find: {url: url + '/:branch_id', method: 'GET'},
            acquired: {url: AppConfig.server + 'adwords/acquired', method: 'GET', isArray: false},
            countries: {url: urlCountry, method: 'GET', isArray: true},
            states: {url: urlCountry + "/MX/states",method: 'GET', isArray: true},
            delete: {url: url + '/:branch_id', method:'DELETE'},
            edit: {url: url + '/:branch_id', method: 'PUT', isArray: false}
        });

    }]);

    app.factory('UploadBrancheImage', ['AppConfig', '$upload', function (AppConfig, $upload) {

        var url = AppConfig.server + "company/branches";

        return {
            upload: function (branchId, selectedImage) {
                return $upload.upload({
                        url: url + "/" + branchId + "/manager/image",
                        method: 'POST',
                        data: { file: selectedImage}
                    });
            }
        };

    }]);


    app.factory('StatsRepository', ['$resource', '$cookies', 'AppConfig', function ($resource, $cookies, AppConfig) {

        var baseUrl = AppConfig.server + "/company/stats/:year/:month/";

        return $resource(baseUrl, {}, {
            callsReceived: {url: baseUrl + 'calls/received', method: 'GET'},
            messagesReceived: {url: baseUrl + 'messages/received', method: 'GET'},
            promotionsRegistered: {url: baseUrl + 'promotions/visited', method: 'GET'},
            promotionsVisited: {url: baseUrl + 'promotions/visited', method: 'GET'}
        });

    }]);

    app.factory('BranchLogsRepository', [ '$resource', '$cookies', 'AppConfig', function ($resource, $cookies, AppConfig) {

        var url = AppConfig.server + "company/branches/:branch_id/";

        return $resource(url, {}, {
            calls: {url: url + 'calls', method: 'GET', isArray: true}
        });

    }]);

})();