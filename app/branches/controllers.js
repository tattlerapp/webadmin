(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.controller("StatisticsBranchController", ['$scope', 'StatsRepository', function ($scope, StatsRepository) {

        $scope.selectedDate = moment().toDate();
        $scope.callStats = {};

        $scope.$watch('selectedDate', function (v) {
            var selected = (typeof v == 'string' || v instanceof String) ? moment(v, "MMMM YYYY") : moment(v);
            var first = selected.startOf('month').clone();
            var last = selected.endOf('month').clone();
            $scope.selectedRange = "(" + first.format("D MMM YYYY") + "-" + last.format("D MMM YYYY") + ")";
        });

        $scope.applyMonthlyFilter = function () {

            var date = moment($scope.selectedDate);

            var month = date.month() + 1;
            var year = date.year();

            var params = {year: year, month: month};

            StatsRepository.callsReceived(params, function (callData) {

                $scope.callStats = {
                    statByDate: [
                        {
                            "key": "Llamadas",
                            "values": callData.days_stats
                        }],
                    statByBranch: callData.branches_stats,
                    totalBranch: callData.total
                };

            });

            StatsRepository.messagesReceived(params, function(messageData){

                $scope.messageStats = {
                    statByDate: [
                        {
                            "key": "Mensajería",
                            "values": messageData.days_stats
                        }],
                    statByBranch: messageData.branches_stats,
                    totalBranch: messageData.total
                };
            });

            StatsRepository.promotionsRegistered(params, function(registeredPromotionsData){

                $scope.registeredPromotionStats = {
                    statByDate: [
                        {
                            "key": "Promociones Registradas",
                            "values": registeredPromotionsData.days_stats
                        }],
                    statByBranch: registeredPromotionsData.branches_stats,
                    totalBranch: registeredPromotionsData.total
                };
            });

            StatsRepository.promotionsVisited(params, function(viewedPromotionsData){

                $scope.viewedPromotionStats = {
                    statByDate: [
                        {
                            "key": "Promociones Vistas",
                            "values": viewedPromotionsData.days_stats
                        }],
                    statByDateDetail: viewedPromotionsData.days_stats,
                    totalBranch: viewedPromotionsData.total
                };
            });


            /*

            var contactsData = StatsRepository.contactsData();
            $scope.contactStats = {
                statByDate: [
                    {
                        "key": "Contactos",
                        "values": contactsData.by_day
                    }],
                statByBranch: contactsData.by_branch,
                totalBranch: $scope.calculateTotal(contactsData.by_branch)
            };
            */

        };

        $scope.calculateTotal = function (data) {
            return _.reduce(data, function (memo, single) {
                return memo + single.count;
            }, 0);
        };

        $scope.xFunction = function () {
            return function (d) {
                return d.unix_time_stamp;
            }
        };

        $scope.yFunction = function () {
            return function (d) {
                return d.count;
            }
        };

        $scope.xAxisTickFormat = function () {
            return function (d) {
                return d3.time.format('%d %b')(new Date(d));  //uncomment for date format
            }
        };

        $scope.applyMonthlyFilter();

    }]);

    app.controller("BranchController", ['$scope', '$http', 'SweetAlert', '$cookies', 'StatsRepository', 'BranchesFactory', '$upload', 'UploadBrancheImage', '$window', '$routeParams', function ($scope, $http, SweetAlert, $cookies, StatsRepository, BranchesFactory, $upload, UploadBrancheImage, $window, $routeParams) {
        $scope.msg = {};
        $scope.branch = {
            country: 'MX',
            state: 'AGU',
            gender: 'Male',
            img: ' ',
            isLandline: true,
            isMessaging: true
        }
        $scope.editBranch = false;
        $scope.submitProgress = false;
        $scope.originalBranch = angular.copy($scope.branch);

        var geo_options = {
         enableHighAccuracy: true,
         maximumAge : 30000,
         timeout : 27000
         };

        var _onGetBranch = function (branch) {

            $scope.branch = {
                            branchName:branch.name,
                            tel:branch.phone_number,
                            street:branch.address.street_line,
                            country:branch.address.country_id,
                            state:branch.address.state_id,
                            email:branch.manager.username,
                            password:branch.manager.password,
                            name:branch.manager.name,
                            lastname:branch.manager.last_name,
                            gender:branch.manager.gender,
                            img:branch.manager.profile_image,
                            startMonday:(branch.schedule.monday.opening_time).slice(0,5),
                            endMonday:(branch.schedule.monday.closing_time).slice(0,5),
                            startTuesday:(branch.schedule.tuesday.opening_time).slice(0,5),
                            endTuesday:(branch.schedule.tuesday.closing_time).slice(0,5),
                            startWednesday:(branch.schedule.wednesday.opening_time).slice(0,5),
                            endWednesday:(branch.schedule.wednesday.closing_time).slice(0,5),
                            startThursday:(branch.schedule.thursday.opening_time).slice(0,5),
                            endThursday:(branch.schedule.thursday.closing_time).slice(0,5),
                            startFriday:(branch.schedule.friday.opening_time).slice(0,5),
                            endFriday:(branch.schedule.friday.closing_time).slice(0,5),
                            startSaturday:(branch.schedule.saturday.opening_time).slice(0,5),
                            endSaturday:(branch.schedule.saturday.closing_time).slice(0,5),
                            startSunday:(branch.schedule.sunday.opening_time).slice(0,5),
                            endSunday:(branch.schedule.sunday.closing_time).slice(0,5),
                            isLandline:branch.allows_phone_calling,
                            isMessaging:branch.allows_messaging
                        };
            $scope.positions = [{lat: branch.location.latitude, lng: branch.location.longitude, centered: true}];
        };

        var _onGetBranchError = function (error) {
            //window.console(error);
        };

        $scope.getBranch=function(id){
            var branch = {branch_id: id};
            BranchesFactory.find(branch, _onGetBranch, _onGetBranchError);                               
        }

        if(typeof ($routeParams.id)!='undefined'){
            $scope.getBranch($routeParams.id);
            $scope.editBranch = true;
        }
        else{
            $scope.positions = [{lat: 20.9667, lng: -89.6167, centered: true}];
        }

        var _onCountries = function (countries) {
            $scope.countries = countries;
            //$scope.$apply();
        };

        var _onCountriesError = function (error) {
            //window.console(error);
        };

        var _onStates = function (states) {
            $scope.states = states;
            //$scope.$apply();
        };

        var _onStatesError = function (error) {
           // window.console(error);
        };

        BranchesFactory.countries({}, {}, _onCountries, _onCountriesError);

        BranchesFactory.states({}, {}, _onStates, _onStatesError);

        $scope.resetBranchForm = function () {
            $scope.branch = angular.copy($scope.originalBranch);
            $scope.registerForm.$setPristine();
            $scope.submitProgress = false;
            $scope.showValidation = false;
            $scope.getlocationInit();
        };

        $scope.checkGeneral = function(isGeneral,startMonday,endMonday){
            if(isGeneral && startMonday!="" && endMonday!="" && typeof startMonday != "undefined" && typeof endMonday != "undefined"){
                $scope.branch.startTuesday=startMonday;
                $scope.branch.endTuesday=endMonday;
                $scope.branch.startWednesday=startMonday;
                $scope.branch.endWednesday=endMonday;
                $scope.branch.startThursday=startMonday;
                $scope.branch.endThursday=endMonday;
                $scope.branch.startFriday=startMonday;
                $scope.branch.endFriday=endMonday;
                $scope.branch.startSaturday=startMonday;
                $scope.branch.endSaturday=endMonday;
                $scope.branch.startSunday=startMonday;
                $scope.branch.endSunday=endMonday;
            }
        }

        var _onSaveBranchMessage = function(data){
            var messageText="Ir a Sucursales";
            var locationConfirm="#/branches";

            if($scope.editBranch){
                var messageText="Ir a la lista de las sucursales";
                var locationConfirm="#/branches/list";
            }

            SweetAlert.swal({
                    title: "Sucursal guardada satisfactoriamente!",
                    text: "",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#428bca",
                    confirmButtonText: messageText
                },
                function () {
                    $window.location.href = locationConfirm;
                }
            );
        }

        var _onSaveBranch = function (branch) {
            var idBranch = "";
            if(!$scope.editBranch){
                idBranch = branch.id;
            }
            else{
                idBranch = $routeParams.id;
            }
            if(typeof $scope.selectedFile != "undefined"){
                UploadBrancheImage.upload(idBranch,$scope.selectedFile).then(
                    function (response) {
                        _onSaveBranchMessage(branch);
                    },
                    _onSaveBranchError);
            }else{
                _onSaveBranchMessage(branch);
            }
            $scope.resetBranchForm();
        };

        var _onSaveBranchError = function (error) {
            SweetAlert.swal("No se pudo guardar la sucursal", "", "error");
            $scope.submitProgress = false;
        };

        $scope.SaveBranch = function (req) {
            if ($scope.registerForm.$invalid) {
                $scope.showValidation = true;
                return;
            }
            $scope.submitProgress = true;
            var branch = {
                              name: req.branchName,
                              phone_number: req.tel,
                              address: {
                                street_line: req.street,
                                country_id: req.country,
                                state_id: req.state
                              },
                              location: {
                                longitude: $scope.positions[0].lng,
                                latitude: $scope.positions[0].lat
                              },
                              manager: {
                                username: req.email,
                                password: req.password,
                                name: req.name,
                                last_name: req.lastname,
                                gender: req.gender//,
                                //profile_image: req.
                              },
                              schedule: {
                                monday: {
                                  opening_time: req.startMonday,
                                  closing_time: req.endMonday
                                },
                                tuesday: {
                                  opening_time: req.startTuesday,
                                  closing_time: req.endTuesday
                                },
                                wednesday: {
                                  opening_time: req.startWednesday,
                                  closing_time: req.endWednesday
                                },
                                thursday: {
                                  opening_time: req.startThursday,
                                  closing_time: req.endThursday
                                },
                                friday: {
                                  opening_time: req.startFriday,
                                  closing_time: req.endFriday
                                },
                                saturday: {
                                  opening_time: req.startSaturday,
                                  closing_time: req.endSaturday
                                },
                                sunday: {
                                  opening_time: req.startSunday,
                                  closing_time: req.endSunday
                                }
                              },
                              allows_phone_calling: req.isLandline,
                              allows_messaging: req.isMessaging
                        };
            if (!$scope.editBranch){
                BranchesFactory.create({}, branch, _onSaveBranch, _onSaveBranchError);
            }
            else{
                var idBranch = {branch_id: $routeParams.id};       
                BranchesFactory.edit(idBranch, branch, _onSaveBranch, _onSaveBranchError);
            }

        };

        $scope.getError = function (error) {
            if (angular.isDefined(error)) {
                if (error.required) {
                    return "Por favor, introduzca un valor.";
                } else if (error.email) {
                    return "Por favor, introduzca un Email v\u00e1lido";
                } else if (error.pattern) {
                    return "Por favor, introduzca un valor v\u00e1lido";
                } else if (error.minlength) {
                    return "No contiene los caracteres minimos(8 para la contrase\u00F1a y 13 para el RFC)";
                } else if (error.passwordVerify) {
                    return "La contrase\u00F1a no coincide";
                }
            }
        };

        $scope.getLocation = function (val) {
            return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    address: val,
                    sensor: false
                }
            }).then(function (response) {
                return response.data.results.map(function (item) {
                    return {
                        location: item.geometry.location,
                        formatted_address: item.formatted_address
                    }
                });
            });
        };

        $scope.getLocation = function (lat, lng) {
            var latitudelongitude = lat + "," + lng
            return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    latlng: latitudelongitude,
                    sensor: false
                }
            }).success(function (response) {
                $scope.address = response.results[0].formatted_address;
                $scope.apply();
            });
        };

        $scope.addMarker = function (event) {
            $scope.positions = [];
            var ll = event.latLng;

            $scope.positions.push({lat: ll.lat(), lng: ll.lng(), centered: false});
            //$scope.getLocation(ll.lat(),ll.lng());
        };
        
        var successgeolocation = function(currentposition) {
            var geoLatitude = currentposition.coords.latitude;
            var geoLongitude = currentposition.coords.longitude;
            $scope.positions[0].lng = geoLongitude;
            $scope.positions[0].lat = geoLatitude;
        }
         
        var errorgeolocation = function (error) {
            SweetAlert.swal("No se puede localizar su ubicación debido a: "+error.code + " : " + error.message, "", "error");
        };
         
        $scope.currentMarkers = function () {
            if(navigator.geolocation){
                $scope.positions = [];
                $scope.positions.push({
                    lat: "current",
                    lng: "current",
                    centered: true
                });
                navigator.geolocation.getCurrentPosition(successgeolocation, errorgeolocation, geo_options);
            }else{
                SweetAlert.swal("El servicio de Geolocalización no es soportado por su explorador.", "", "error");
            }
        };

        $scope.changeAddress = function (details) {
            if (typeof details != 'undefined') {
                $scope.positions = [];
                $scope.positions.push({
                    lat: details.geometry.location.k,
                    lng: details.geometry.location.D,
                    centered: true
                });
                //$scope.address = details.formatted_address;
                //$scope.apply();
            }
        };

        $scope.$watch('branch.detailsAddress', function (newValue, oldValue) {
            $scope.changeAddress(newValue);
        });

        $scope.$watch('detailsAddress', function (newValue, oldValue) {
            $scope.changeAddress(newValue);
        });


        var handleFileSelect = function (evt) {
            var file = evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.branch.img = evt.target.result;
                });
            };
            reader.readAsDataURL(file);
            $scope.hideImages = true;
        };

        angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);
        
        $scope.onFileSelect = function ($files) {
            $scope.selectedFile = $files[0];//set a single file
        };

        $scope.getlocationInit = function () {
          $scope.count = '(waiting for result)';
          $scope.mapResults = [];
          
            $scope.geocoder = new google.maps.Geocoder();

            var paramGeocode = 'Aguascalientes, MX';
            $scope.geocoder.geocode( { 'address': paramGeocode}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                $scope.count = results.length;
                $scope.mapResults = results[0];
                $scope.changeAddress(results[0]);
              } else {
                $scope.status = "Geocode was not successful: " + status;
              }
            $scope.$apply();
            });
        };

        $scope.getlocationInit();

        $scope.getlocation = function () {
          $scope.count = '(waiting for result)';
          $scope.mapResults = [];
          
            $scope.geocoder = new google.maps.Geocoder();

            var paramGeocode = $scope.branch.state + ', ' + $scope.branch.country;
            $scope.geocoder.geocode( { 'address': paramGeocode}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                $scope.count = results.length;
                $scope.mapResults = results[0];
                $scope.changeAddress(results[0]);
              } else {
                $scope.status = "Geocode was not successful: " + status;
              }
            $scope.$apply();
            });
        };

    }]);
    app.controller("BranchesListController", ['$scope', '$http', 'SweetAlert', 'BranchesFactory', '$location', function ($scope, $http, SweetAlert, BranchesFactory, $location) {
        $scope.gridBranches = {enableCellEditOnFocus: true};
        $scope.msg = {};
        $scope.countDelete = 0;
        $scope.countDeleteError = 0;

        $scope.gridBranches.columnDefs = [
            {name: 'name', displayName: 'Sucursal',enableCellEdit: false},
            {name: 'manager.name', displayName: 'Nombre',enableCellEdit: false},
            {name: 'manager.last_name', displayName: 'Apellidos',enableCellEdit: false},
            {name: 'manager.username', displayName: 'usuario',enableCellEdit: false},
            {name: 'phone_number', displayName: 'Tel\u00E9fono',enableCellEdit: false},
            /*{name: 'Perfil', cellTemplate:'<img src="{{row.entity.manager.profile_image}}" alt="Sin foto" width="42" height="42"">',enableCellEdit: false},*/
            { name: 'Editar',
             cellTemplate:'<button class="btn primary" ng-click="grid.appScope.showMe(row.entity.id)">Editar</button>',enableCellEdit: false}
             /*,    {{row.getProperty(\'id\')}}
            {name: 'address', displayName: 'Direcci\u00F3n', width: '45%'}*/
        ];

        $scope.showMe = function(id){
            var url = '/branches/' + id;
            $location.path(url);
        };

        $scope.refreshList=function(){
            BranchesFactory.all({}, function (branches) {
                $scope.gridBranches.data = branches;
                $scope.positionsmap = [];
                angular.forEach(branches, function(value, key) {
                    $scope.positionsmap.push({
                        lat: value.location.latitude,
                        lng: value.location.longitude,
                        centered: true,
                        name:value.name,
                        managername:value.manager.name,
                        managerlastname:value.manager.last_name,
                        managerusername:value.manager.username,
                        phone_number:value.phone_number,
                        id:value.id
                    });

                });
            });
        }

        $scope.refreshList();

        $scope.addRow = function (name, phone_number) {
            $scope.gridBranches.data.push(
                {
                    "name": name,
                    "phone_number": phone_number//,
                    //"address": address
                });
        };

        $scope.gridBranches.onRegisterApi = function (gridApi) {
            $scope.gridApi = gridApi;
            gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                $scope.msg.lastCellEdited = 'edited row id:' + rowEntity.id + ' Column:' + colDef.name + ' newValue:' + newValue + ' oldValue:' + oldValue;
                $scope.$apply();
            });
        };

        var _onDeleteBranch = function (data) {
            
        };

        var _onDeleteBranchError = function (error) {
            SweetAlert.swal("No se pudo eliminar alguna sucursal", "", "error");
        };

        $scope.DeleteBranches = function () {
            var selectedRows = $scope.gridApi.selection.getSelectedRows();

            if (selectedRows.length > 0){
                SweetAlert.swal({
                        title: "Eliminar sucursal",
                        text: "¿Seguro que deseas eliminar los sucursales seleccionados?",
                        type: "info",
                        showCancelButton: true,
                        cancelButtonText: "No",
                        confirmButtonColor: "#428bca",
                        confirmButtonText: "Eliminar"
                    },
                    function () {
                        angular.forEach(selectedRows, function(value, key) {
                            var branch = {branch_id: value.id}
                            ;
                            BranchesFactory.delete(branch, _onDeleteBranch, _onDeleteBranchError);                               
                        });
                        $scope.refreshList();
                    }
                );
            }

        };
    }]);
    app.config(function ($routeProvider) {
        $routeProvider
            .when("/branches", {
                templateUrl: "branches/partials/index.html",
                css: "branches/branches.css",
                controller: "BranchController"
            })
            .when("/branches/statistics", {
                templateUrl: "branches/partials/statistics.html",
                css: "branches/branches.css",
                controller: "StatisticsBranchController"
            })
            .when("/branches/logs", {
                templateUrl: "branches/partials/logs.html",
                css: "branches/branches.css",
                controller: "BranchesLogController"
            })
            .when("/branches/list", {
                templateUrl: "branches/partials/list.html",
                css: "branches/branches.css",
                controller: "BranchesListController"
            })
            .when("/branches/:id",{
                templateUrl: "branches/partials/index.html",
                css: "branches/branches.css",
                controller: "BranchController"
            });
    });


})();