(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.controller('CropPhotoController', function ($http, $scope, FileUtils) {

        var files = $scope.ngDialogData.files;

        $scope.profileResultImage = '';

        $scope.initial = function () {

            var file = files[0];

            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.profileImage = evt.target.result;
                });
            };
            reader.readAsDataURL(file);

        };

        $scope.select = function () {
            $scope.closeThisDialog($scope.profileResultImage);
        };

    });

})();