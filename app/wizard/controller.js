(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.controller("WizardController", function ($rootScope, $scope, $modal, $window, $log, SweetAlert, FileUtils, ngDialog,
                                                 CompanyRepository, CompanyService, BranchesFactory, PlansRepository,
                                                 AdwordsRepository, PromotionRepository, PromotionImageRepository, ConektaService) {

        //Tooltips
        $scope.new_branch_tooltip = {title: 'Registra por lo menos una sucursal, más adelante podrás registrar otras!'};
        $scope.tooltip_presupuesto = {title: 'Elige el presupuesto que más se ajuste a tus necesidades.'};
        $scope.tooltip_minutos = {title: 'Esta es la cantidad máxima de minutos con la que contarás, aumenta las intenciones de compra de tus clientes con esta opción'};
        $scope.tooltip_promociones = {title: 'Esta es la cantidad máxima de usuarios únicos que podrán ver tus promociones'};
        $scope.tooltip_mensajeria = {title: 'La mensajería es gratis e ilimitada siempre'};
        $scope.tooltip_alcance_minimo = {title: 'Te garantizamos que no recibirás menos y lo podrás verificar en las estadísticas'};
        $scope.tooltip_alcance_maximo = {title: 'Este será tu alcance que tendrás como máximo'};
        $scope.tooltip_ingreso_promedio = {title: '¿Cuánto consume un cliente a tu negocio en promedio?'};
        $scope.tooltip_retorno_minimo = {title: 'Este será el mínimo de tu retorno de inversión en Tattler.'};
        $scope.tooltip_inversion = {title: 'Esta es la inversión en Tattler Adwords.'};

        //Constants
        $scope.step = 1;
        $scope.company = {};

        $scope.promotion = {};
        $scope.promotion.image = {};
        $scope.promotion.image.dataUrl = "http://th05.deviantart.net/fs71/PRE/i/2012/176/c/7/free_pizza_everyday_by_michaeltuan97-d54vuqb.jpg";

        $scope.promotion = {};
        $scope.promotion.start_date = new Date();
        $scope.promotion.expiration_date = new Date();
        $scope.promotion.issued_coupons = 1;

        var _setStep = function (step) {
            $scope.step = step;
            $scope.showError = false;
        };

        var _isStep = function (step) {
            return $scope.step == step;
        };

        var _generateThumb = function (model, files) {
            FileUtils.generateThumb(model, files);
        };

        var _generateCrop = function (model, files) {

            if (files == undefined || files.length == 0)
                return;

            var dialog = ngDialog.open({
                template: "picture-modal",
                //templateUrl: "wizard/partials/picture_modal.html",
                className: 'ngdialog-theme-crop',
                controller: "CropPhotoController",
                data: {files: files},
                closeByDocument: true,
                closeByEscape: true
            });

            dialog.closePromise.then(function (data) {
                var updateImageUri = data.value;
                if (updateImageUri == undefined)
                    return;
                $scope.company.updated_profile_image = updateImageUri;
            });

        };

        var _updateCompany = function () {

            var description = $scope.company.description;
            var profileUri = $scope.company.updated_profile_image;
            var backgroundUri = $scope.company.updated_background_image.dataUrl;

            CompanyService.updateCompany(profileUri, backgroundUri, description, function () {
                _setStep(2);
            }, function () {
                SweetAlert.swal("No se pudo actualizar empresa", "", "error");
            });

        };

        var _deleteBranch = function (branch) {
            SweetAlert.swal({
                    title: "Eliminar sucursal",
                    text: "¿Seguro que deseas eliminar esta sucursal?",
                    type: "info",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#428bca",
                    confirmButtonText: "Eliminar"
                },
                function () {
                    BranchesFactory.delete({branch_id: branch.id}, function (response) {
                            refreshBranches();
                        },
                        function (error) {
                            SweetAlert.swal(error.data.Message, "", "error");
                        });
                }
            );
        };

        var _finishBranchEdition = function () {


            if ($scope.branches.length == 0) {
                SweetAlert.swal("Da de alta al menos una sucursal", "", "error");
                return;
            }

            initPromotionCreate();

        };

        var _newBranch = function () {

            var modalInstance = $modal.open({
                templateUrl: 'branch-modal',
                controller: 'BranchModalController',
                size: 'lg'
            });

            modalInstance.result.then(function (createdBranch) {
                refreshBranches();
            });

        };

        var _acquireAdwords = function () {

            ConektaService.getCardToken($scope.card, function (token) {

                var adword = {token_id: token, plan_id: $scope.selectedPlan.id};

                AdwordsRepository.create({}, adword, function (response) {
                    _finishWizard();
                }, function (error) {
                    SweetAlert.swal(error.data.Message, "", "error");
                });

            }, function (message) {
                SweetAlert.swal(message, "", "error");
            });

        };

        var _createPromotion = function (promotionDetails) {

            var branchesIdArray = _.chain($scope.promotionBranches).where(function (branch) {
                return branch.ticked == true;
            }).pluck('id').value();

            if ($scope.promotionForm.$invalid || $scope.promotion.image == undefined) {
                showErrorMessage("Llena los campos de la promoción (incluyendo imagen) primero.");
                return;
            }

            var promotion = {
                title: promotionDetails.title,
                expiration_date: promotionDetails.expiration_date,
                start_date: promotionDetails.start_date,
                branches: branchesIdArray,
                issued_coupons: promotionDetails.issued_coupons
            };

            PromotionRepository.create({}, promotion, function (promotion) {

                var imageUri = $scope.promotion.image == undefined ? undefined : $scope.promotion.image.dataUrl;
                PromotionImageRepository.upload(promotion.id, imageUri).then(function (response) {
                    _setStep(4);
                }, function (error) {
                    showErrorMessage(error.data.Message);
                });
            }, function (error) {
                showErrorMessage(error.data.Message);
            });

        };

        var _finishWizard = function () {
            $rootScope.onWizard = false;
            $window.location.href = '#/dashboard';
        };

        var refreshBranches = function () {
            BranchesFactory.all({}, function (branches) {
                $scope.branches = branches;
            });
        };

        var refreshPlans = function () {
            PlansRepository.all({}, function (plans) {
                $scope.plans = plans;
                $scope.selectedPlan = $scope.plans[0];
                $scope.selectedIncome = $scope.selectedPlan.mean_incomes[0];
            });
        };

        var initPromotionCreate = function () {
            $scope.promotionBranches = _.map($scope.branches, function (branch) {
                return {
                    id: branch.id,
                    name: branch.name,
                    ticked: true
                }
            });
            _setStep(3);
        };

        var showErrorMessage = function (message) {
            SweetAlert.swal(message, "", "error");
        };

        $scope.$watch('selectedPlan', function (plan) {
            if (plan == undefined) return;
            $scope.selectedIncome = plan.mean_incomes[0];
        });

        var getProfile = function () {
            CompanyRepository.current({}, function (current) {
                $scope.company = current;

                //$scope.company.profile_image = null;
                //$scope.company.background_image = null;

                $scope.company.updated_background_image = {};
            }, function (error) {
            });
        };

        $scope.updateCompany = _updateCompany;
        $scope.isStep = _isStep;
        $scope.setStep = _setStep;
        $scope.generateCrop = _generateCrop;
        $scope.generateThumb = _generateThumb;
        $scope.deleteBranch = _deleteBranch;
        $scope.finishBranchEdition = _finishBranchEdition;
        $scope.newBranch = _newBranch;
        $scope.acquireAdwords = _acquireAdwords;
        $scope.finishWizard = _finishWizard;
        $scope.createPromotion = _createPromotion;

        //On Init
        refreshBranches();
        refreshPlans();
        getProfile();


    });

    app.config(function ($routeProvider) {
        $routeProvider
            .when("/wizard", {
                templateUrl: "wizard/wizard.html",
                css: "wizard/wizard.css",
                controller: "WizardController"
            });
    });

})();