(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.controller('BranchModalController', function ($scope, $modalInstance, SweetAlert, BranchesFactory, $http) {
        $scope.positions = [{lat: 20.9667, lng: -89.6167, centered: true}];

        $scope.branch = {};
        $scope.branch.location = {latitude: 20.9667, longitude: -89.6167};
        $scope.branch.allows_phone_calling = true;
        $scope.branch.allows_messaging = true;
        $scope.branch.address = {};
        $scope.branch.manager = {};
        $scope.branch.manager.gender = "Male";
        $scope.branch.schedule = {
                                monday: {
                                  opening_time: "",
                                  closing_time: ""
                                },
                                tuesday: {
                                  opening_time: "",
                                  closing_time: ""
                                },
                                wednesday: {
                                  opening_time: "",
                                  closing_time: ""
                                },
                                thursday: {
                                  opening_time: "",
                                  closing_time: ""
                                },
                                friday: {
                                  opening_time: "",
                                  closing_time: ""
                                },
                                saturday: {
                                  opening_time: "",
                                  closing_time: ""
                                },
                                sunday: {
                                  opening_time: "",
                                  closing_time: ""
                                }
                              };
        /*$scope.branch.schedule.tuesday.opening_time = "";
        $scope.branch.schedule.wednesday.opening_time = "";
        $scope.branch.schedule.wednesday.opening_time = "";
        $scope.branch.schedule.thursday.opening_time = "";
        $scope.branch.schedule.thursday.opening_time = "";
        $scope.branch.schedule.friday.opening_time = "";
        $scope.branch.schedule.friday.opening_time = "";
        $scope.branch.schedule.saturday.opening_time = "";
        $scope.branch.schedule.saturday.opening_time = "";
        $scope.branch.schedule.sunday.opening_time = "";
        $scope.branch.schedule.sunday.opening_time = "";*/

        var geo_options = {
             enableHighAccuracy: true,
             maximumAge : 30000,
             timeout : 27000
         };

        BranchesFactory.countries({}, {}, function (countries) {
            $scope.countries = countries;
            $scope.branch.address.country = $scope.countries[0];
        });

        BranchesFactory.states({}, {}, function (states) {
            $scope.states = states;
            $scope.branch.address.state = $scope.states[0];
        });

        $scope.checkGeneral = function (isGeneral, startMonday, endMonday) {
            if (isGeneral && startMonday != "" && endMonday != "" && typeof startMonday != "undefined" && typeof endMonday != "undefined") {
                $scope.branch.schedule.tuesday.opening_time = startMonday;
                $scope.branch.schedule.tuesday.closing_time = endMonday;
                $scope.branch.schedule.wednesday.opening_time = startMonday;
                $scope.branch.schedule.wednesday.closing_time = endMonday;
                $scope.branch.schedule.thursday.opening_time = startMonday;
                $scope.branch.schedule.thursday.closing_time = endMonday;
                $scope.branch.schedule.friday.opening_time = startMonday;
                $scope.branch.schedule.friday.closing_time = endMonday;
                $scope.branch.schedule.saturday.opening_time = startMonday;
                $scope.branch.schedule.saturday.closing_time = endMonday;
                $scope.branch.schedule.sunday.opening_time = startMonday;
                $scope.branch.schedule.sunday.closing_time = endMonday;
            }
        };

        $scope.AddBranch = function () {

            var _onCreateBranchError = function (error) {
                SweetAlert.swal("No se pudo crear la sucursal", "", "error");
            };

            var branchObject = angular.copy($scope.branch);

            branchObject.address.country_id = branchObject.address.country.id;
            branchObject.address.state_id = branchObject.address.state.id;

            BranchesFactory.create({}, branchObject, function (branch) {
                $modalInstance.close(branchObject);
            }, _onCreateBranchError);

        };

        $scope.setlocation = function(lat,lng){
            $scope.branch.location = {latitude: lat, longitude: lng};
        }

        $scope.getlocation = function () {
          $scope.count = '(waiting for result)';
          $scope.mapResults = [];
          
            $scope.geocoder = new google.maps.Geocoder();

            var paramGeocode = $scope.branch.address.state.name + ', ' + $scope.branch.address.country.name;
            $scope.geocoder.geocode( { 'address': paramGeocode}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                $scope.count = results.length;
                $scope.mapResults = results[0];
                $scope.changeAddress(results[0]);
              } else {
                $scope.status = "Geocode was not successful: " + status;
              }
            $scope.$apply();
            });
        };

        $scope.getlocationInit = function () {
          $scope.count = '(waiting for result)';
          $scope.mapResults = [];
          
            $scope.geocoder = new google.maps.Geocoder();

            var paramGeocode = 'Aguascalientes, MX';
            $scope.geocoder.geocode( { 'address': paramGeocode}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                $scope.count = results.length;
                $scope.mapResults = results[0];
                $scope.changeAddress(results[0]);
              } else {
                $scope.status = "Geocode was not successful: " + status;
              }
            $scope.$apply();
            });
        };

        $scope.getlocationInit();

        $scope.getLocation = function (lat, lng) {
            var latitudelongitude = lat + "," + lng
            return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    latlng: latitudelongitude,
                    sensor: false
                }
            }).success(function (response) {
                $scope.address = response.results[0].formatted_address;
                $scope.apply();
            });
        };

        $scope.addMarker = function (event) {
            $scope.positions = [];
            var ll = event.latLng;
            $scope.positions.push({lat: ll.lat(), lng: ll.lng(), centered: false});
            $scope.setlocation(ll.lat(),ll.lng());
        };

        var successgeolocation = function(currentposition) {
            var geoLatitude = currentposition.coords.latitude;
            var geoLongitude = currentposition.coords.longitude;

            $scope.positions[0].lng = geoLongitude;
            $scope.positions[0].lat = geoLatitude;

            $scope.setlocation(geoLatitude,geoLongitude);
        }
         
        var errorgeolocation = function (error) {
            SweetAlert.swal("No se puede localizar su ubicación debido a: "+error.code + " : " + error.message, "", "error");
        };

        $scope.currentMarkers = function () {
            $scope.positions = [];
            $scope.positions = [{lat: "current", lng: "current", centered: true}];

            if(navigator.geolocation){
                $scope.positions.push({
                    lat: "current",
                    lng: "current",
                    centered: true
                });
                navigator.geolocation.getCurrentPosition(successgeolocation, errorgeolocation, geo_options);
            }else{
                SweetAlert.swal("El servicio de Geolocalización no es soportado por su explorador.", "", "error");
            }
        };

        $scope.changeAddress = function (details) {
            if (typeof details != 'undefined') {
                var latitude = details.geometry.location.k;
                var longitude = details.geometry.location.D;
                $scope.positions = [];
                $scope.positions.push({
                    lat: latitude,
                    lng: longitude,
                    centered: true
                });
                $scope.setlocation(latitude,longitude);
            }
        };

        $scope.$watch('branch.detailsAddress', function (newValue, oldValue) {
            $scope.changeAddress(newValue);
        });

        $scope.$watch('detailsAddress', function (newValue, oldValue) {
            $scope.changeAddress(newValue);
        });

    });

})();