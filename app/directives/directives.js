(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.directive("passwordVerify", function () {
        return {
            require: "ngModel",
            scope: {
                passwordVerify: '='
            },
            link: function (scope, element, attrs, ctrl) {
                scope.$watch(function () {
                    var combined;

                    if (scope.passwordVerify || ctrl.$viewValue) {
                        combined = scope.passwordVerify + '_' + ctrl.$viewValue;
                    }
                    return combined;
                }, function (value) {
                    if (value) {
                        ctrl.$parsers.unshift(function (viewValue) {
                            var origin = scope.passwordVerify;
                            if (origin !== viewValue) {
                                ctrl.$setValidity("passwordVerify", false);
                                return undefined;
                            } else {
                                ctrl.$setValidity("passwordVerify", true);
                                return viewValue;
                            }
                        });
                    }
                });
            }
        };
    });

    app.directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });

    app.directive('ngTap', function () {
        return function (scope, element, attrs) {
            var tapping;
            tapping = false;
            element.bind('touchstart', function (e) {
                element.addClass('active');
                tapping = true;
            });
            element.bind('touchmove', function (e) {
                element.removeClass('active');
                tapping = false;
            });
            element.bind('touchend', function (e) {
                element.removeClass('active');
                if (tapping) {
                    scope.$apply(attrs['ngTap'], element);
                }
            });
        };
    });

    app.directive('datetimez', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                element.datetimepicker({
                    format: "MM-yyyy",
                    viewMode: "months",
                    minViewMode: "months",
                    pickTime: false
                }).on('changeDate', function (e) {
                    ngModelCtrl.$setViewValue(e.date);
                    scope.$apply();
                });
            }
        };
    });

    app.directive("viewContainer", function () {
        return {
            link: function postLink(scope, element, attrs) {
                scope.$on("$routeChangeSuccess", function () {
                    $(window).scrollTop(0);
                });

                scope.$on("$viewContentLoaded", function () {
                    $(".carousel").carousel();
                });
            }
        };
    });

})();