(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.factory('PromotionRepository', function ($resource, $cookies, AppConfig) {

        var url = AppConfig.server + "company/promotions";

        return $resource(url, {}, {
            create: {method: 'POST'},
            update: {url: url + "/:id", method: 'PUT'},
            all: {method: 'GET', isArray: true},
            get: {url: url + "/:id", method: 'GET', isArray: false},
            delete: {url: url + "/:id", method: 'DELETE'},
            extending: {url: url + "/:id/extending", method: 'PUT'},
            increaseCoupons: {url: url + "/:id/coupons/increasing", method: 'PUT'}
        });

    });

    app.factory('PromotionImageRepository', function ($upload, $cookies, AppConfig, FileUtils) {

        var url = AppConfig.server + "company/promotions";

        return {
            upload: function (promotionId, imageUri) {

                var data = FileUtils.dataURItoBlob(imageUri);

                return $upload.upload({
                    url: url + "/" + promotionId,
                    method: 'POST',
                    file: data.blob,
                    fileName: data.filename
                });

            }
        };

    });

})();