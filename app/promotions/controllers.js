(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.controller("PromotionsController", function ($scope, $window, SweetAlert, ngDialog, PromotionRepository) {

        var vm = this;

        $scope.totalViewScope = _.reduce($scope.promotions, function (sum, promotion) {
            return sum + promotion.visits_count;
        }, 0);

        vm.refreshPromotions = function () {

            PromotionRepository.all({}, function (promotions) {
                $scope.promotions = promotions;
                $scope.totalViewScope = _.reduce($scope.promotions, function (sum, promotion) {
                    return sum + promotion.visits_count;
                }, 0);
            });

        };

        $scope.edit = function (promotion) {
            $window.location.href = '#/promotions/' + promotion.id + '/edit';
        };

        $scope.delete = function (promotion) {

            SweetAlert.swal({
                    title: "Eliminar promoción",
                    text: "¿Seguro que deseas eliminar esta promoción?",
                    type: "info",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#428bca",
                    confirmButtonText: "Eliminar"
                },
                function () {
                    PromotionRepository.delete({id: promotion.id}, function (response) {
                            vm.refreshPromotions();
                        },
                        function (error) {
                            SweetAlert.swal(error.data.Message, "", "error");
                        });
                }
            );


        };

        $scope.increaseExpiration = function(promotion){

            var dialog = ngDialog.open({
                template: 'expand-promotion-modal',
                className: 'ngdialog-theme-default',
                controller: 'IncreaseExpirationCtrl',
                data: { promotion: promotion }
            });

            dialog.closePromise.then(function (data) {

                if(data != undefined && data.value != undefined){
                    vm.refreshPromotions();
                }

            });
        };

        $scope.increaseCoupons = function(promotion){

            var dialog = ngDialog.open({
                template: 'expand-promotion-codes-modal',
                className: 'ngdialog-theme-default',
                controller: 'IncreaseCouponsCtrl',
                data: { promotion: promotion }
            });

            dialog.closePromise.then(function (data) {

                if(data != undefined && data.value != undefined){
                    vm.refreshPromotions();
                }

            });
        };

        vm.refreshPromotions();

    });

    app.controller('IncreaseExpirationCtrl', function ($scope, ngDialog, SweetAlert, PromotionRepository) {

        $scope.promotion = $scope.ngDialogData.promotion;

        $scope.promotionMinDate = Date.parse($scope.promotion.expiration_date);

        $scope.newExpirationDate = $scope.promotionMinDate;

        $scope.updateExpirationDate = function () {

            var data = {
                promotion_id: $scope.promotion.id,
                expiration_date: $scope.newExpirationDate
            };

            PromotionRepository.extending({id: $scope.promotion.id}, data, function(response){

                $scope.promotion.expiration_date = $scope.newExpirationDate;

                SweetAlert.swal("Fecha de vencimiento actualizada", "", "success");
                $scope.closeThisDialog($scope.promotion);

            }, function(error){
                SweetAlert.swal("No se pudo actualizar vigencia", "", "error");
            });

        };

    });

    app.controller('IncreaseCouponsCtrl', function ($scope, ngDialog, SweetAlert, PromotionRepository) {

        $scope.promotion = $scope.ngDialogData.promotion;
        $scope.increaseBy = 0;

        $scope.increaseCoupons = function () {

            console.log($scope.increaseBy);

            var data = {
                promotion_id: $scope.promotion.id,
                increase_by: $scope.increaseBy
            };

            PromotionRepository.increaseCoupons({id: $scope.promotion.id}, data, function(response){
                console.log(response);
                SweetAlert.swal("Cantidad de cupones incrementada", "", "success");
                $scope.closeThisDialog($scope.promotion);
            }, function(error){
                SweetAlert.swal("No se pudo actualizar cantidad de cupones", "", "error");
            });

        };

    });

    app.controller("NewPromotionController",
        function ($scope, $http, $cookies, $window, SweetAlert, FileUtils,
                  AdwordsRepository, PromotionRepository, PromotionImageRepository, BranchesFactory) {

            var vm = this;

            $scope.promotion = {};
            $scope.promotion.start_date = new Date();
            $scope.promotion.expiration_date = new Date();
            $scope.promotion.issued_coupons = 1;

            BranchesFactory.all({}, function (branches) {
                $scope.branches = _.map(branches, function (branch) {
                    return {
                        id: branch.id,
                        name: branch.name,
                        ticked: true
                    }
                });
            });

            AdwordsRepository.acquired({}, function (adword) {},
                function (error) {
                    SweetAlert.swal({
                            title: "Para ingresar promociones y contar con los servicios de publicidad y telefonía, deberás adquirir los servicios de Tattler Adwords",
                            text: "",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#428bca",
                            confirmButtonText: "Crear campaña"
                        },
                        function () {
                            $window.location.href = '#/adwords/new';
                        }
                    );
                });

            var _onCreatePromotionError = function (error) {
                SweetAlert.swal(error.data.Message, "", "error");
            };

            var _onCreatePromotion = function (promotion) {

                var imageUri = $scope.promotion.image == undefined? undefined : $scope.promotion.image.dataUrl;
                PromotionImageRepository.upload(promotion.id, imageUri).then(function (response) {
                        SweetAlert.swal({
                                title: "Promoción dada de alta!",
                                text: "",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#428bca",
                                confirmButtonText: "Ir a promociones"
                            },
                            function () {
                                $window.location.href = '#/promotions';
                            }
                        );
                    },
                    _onCreatePromotionError);

            };

            var formInvalid = function () {
                //TODO: Validate file selection
                return $scope.promotionForm.$invalid || $scope.promotion.image == undefined;
            };

            var _createPromotion = function (promotionDetails) {

                var branchesIdArray = _.chain($scope.branches).where(function (branch) {
                    return branch.ticked == true;
                }).pluck('id').value();

                if (formInvalid()) {
                    SweetAlert.swal("Llena los campos de la promoción (incluyendo imagen) primero.", "", "error");
                    return;
                }

                var promotion = {
                    title: promotionDetails.title,
                    expiration_date: promotionDetails.expiration_date,
                    start_date: promotionDetails.start_date,
                    branches: branchesIdArray,
                    issued_coupons: promotionDetails.issued_coupons
                };

                PromotionRepository.create({}, promotion, _onCreatePromotion, _onCreatePromotionError);

            };

            var _generateThumb = function(model, files){
                FileUtils.generateThumb(model, files);
            };

            $scope.generateThumb = _generateThumb;
            $scope.createPromotion = _createPromotion;

        });

    app.controller("EditPromotionController",
        function ($routeParams, $scope, $http, $cookies, $window, SweetAlert, FileUtils,
                  AdwordsRepository, PromotionRepository, PromotionImageRepository, BranchesFactory) {


            BranchesFactory.all({}, function (branches) {
                $scope.branches = _.map(branches, function (branch) {
                    return {
                        id: branch.id,
                        name: branch.name,
                        ticked: true
                    }
                });
            });

            var _onPromotionUpdateError = function (error) {
                SweetAlert.swal(error.data.Message, "", "error");
            };

            var showSuccessMessage = function () {
                SweetAlert.swal({
                        title: "Promoción actualizada!",
                        text: "",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#428bca",
                        confirmButtonText: "Ir a promociones"
                    },
                    function () {
                        $window.location.href = '#/promotions';
                    }
                );
            };

            var _onPromotionUpdated = function (promotion) {

                var imageUri = $scope.promotion.updated_image.dataUrl;

                if(imageUri == undefined){
                    showSuccessMessage();
                    return;
                }

                PromotionImageRepository.upload($scope.promotion.id, imageUri).then(function (response) {
                        showSuccessMessage();
                    },
                    _onPromotionUpdateError);

            };

            var _updatePromotion = function (promotion) {

                var branchesIdArray = _.chain($scope.branches).where(function (branch) {
                    return branch.ticked == true;
                }).pluck('id').value();

                promotion.branches = branchesIdArray;

                PromotionRepository.update({id: $scope.promotion.id}, promotion, _onPromotionUpdated, _onPromotionUpdateError);

            };

            var _generateThumb = function(model, files){
                FileUtils.generateThumb(model, files);
            };

            $scope.generateThumb = _generateThumb;
            $scope.update = _updatePromotion;

            //Init
            PromotionRepository.get({id: $routeParams.id}, function (promotion) {
                $scope.promotion = promotion;
                $scope.promotion.updated_image = {};
            });

        });


    app.config(function ($routeProvider) {
        $routeProvider
            .when("/promotions", {
                templateUrl: "promotions/partials/index.html",
                css: "promotions/promotion.css",
                controller: "PromotionsController"
            }).when("/promotions/:id/edit", {
                templateUrl: "promotions/partials/edit.html",
                css: "promotions/promotion.css",
                controller: "EditPromotionController"
            }).when("/promotions/new", {
                templateUrl: "promotions/partials/new.html",
                css: "promotions/promotion.css",
                controller: "NewPromotionController"
            });
    });

})();