(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.controller("DashboardController",
        function ($scope, ContactsRepository) {

            ContactsRepository.all({}, function (contacts) {
                $scope.contacts = contacts.length;
            });

        });

})();