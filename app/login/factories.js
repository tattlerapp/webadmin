(function () {

    'use strict';

    var app = angular.module("tattlerApp");
    app.factory('LoginRepository', ['$resource', 'AppConfig', function ($resource, AppConfig) {

        var url = AppConfig.server + "accounts/login";
        var urlActive = AppConfig.server + "company/account";

        return $resource(url, {}, {
            login: {url: url,method: 'POST',headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
            active: {url: urlActive + "/activation",method: 'PUT', isArray: false},
            recover: {url: urlActive + "/recovery-token",method: 'POST'},
            recoverAccount: { url: urlActive + "/recovery-token",method: 'PUT', isArray: false}
        });

    }]);

})();