(function() {

    'use strict';

    var app = angular.module("tattlerApp");

    app.controller("LoginController",[ '$scope','$routeParams','$http','$location', 'authService', '$rootScope', 'LoginRepository', 'SweetAlert', function ($scope, $routeParams, $http, $location, authService, $rootScope, LoginRepository, SweetAlert) {

        $scope.loginData = {
            userName: "",
            password: "",
            useRefreshTokens: true
        };

        $scope.originalUser = angular.copy($scope.loginData);

        $scope.errormessage = function(message){
            SweetAlert.swal(message, "", "error");
        }

        var _onActivation = function (data) {
            if(data.was_successful){
                $scope.welcome = "Bienvenido";            
                SweetAlert.swal("Su cuenta fue activada con éxito!", "", "success");
            }else{
                $scope.welcome = "Cuenta no Activada";
                $scope.errormessage("Lo sentimos, no se realizó la activación");
            }
        };

        var _onActivationError = function (error) {
            $scope.welcome = "Cuenta no Activada"; 
            $scope.submitProgress = false;
            $scope.errormessage(error.data.Message);
        };

        if(typeof ($routeParams.token)!='undefined' && typeof $scope.flagActive =='undefined' ){
            
             var activation = {
                activation_code: $routeParams.token
            };

           LoginRepository.active({}, activation, _onActivation, _onActivationError);

            $scope.flagActive = true;
        }
        else{
            $scope.welcome = "Bienvenido";
        }

        $scope.resetUserForm = function (isOk) {
            $scope.successfull = isOk;
            $scope.showMessage = true;
            $scope.loginData.password = "";
            $scope.userForm.$setPristine();
            $scope.submitProgress = false;
            $scope.showValidation = false;
        };

        $scope.UserLogin = function () {
            if (!$scope.userForm.$invalid) {
                $scope.submitProgress = true;

                authService.login($scope.loginData).then(function (response) {
                },
                 function (err) {
                     $scope.resetUserForm(false);
                     $scope.message = err.error_description;
                 });            
            } else {
                $scope.showValidation = true;
            }
        };

        $scope.getError = function (error) {
            if (angular.isDefined(error)) {
                if (error.required) {
                    return "Por favor, introduzca un valor.";
                } else if (error.email) {
                    return "Por favor, introduzca un Email v\u00e1lido";
                } else if (error.pattern) {
                    return "Por favor, introduzca un valor v\u00e1lido";
                } else if (error.minlength) {
                    return "El password debe ser mayor a 8 caracteres";
                }
            }
        }
    }]);
    app.controller("RecoverController", function ($scope, $http, LoginRepository) {

        $scope.originalUser = angular.copy($scope.user);

        $scope.resetUserForm = function (isOk) {
            $scope.showMessage = true;
            $scope.successfull = isOk;
            $scope.user = angular.copy($scope.originalUser);
            $scope.userForm.$setPristine();
            $scope.submitProgress = false;
            $scope.showValidation = false;
        };

        var _onRecovermail = function (data) {
            $scope.resetUserForm(true);
        };

        var _onRecovermailError = function (error) {
            $scope.resetUserForm(false);
        };

        $scope.recover = function (userDetails) {
            if (!$scope.userForm.$invalid) {
                var recovermail = {
                    email: userDetails.email
                };

               LoginRepository.recover({}, recovermail, _onRecovermail, _onRecovermailError);
            } else {
                $scope.showValidation = true;
            }
        };

        $scope.getError = function (error) {
            if (angular.isDefined(error)) {
                if (error.required) {
                    return "Por favor, introduzca un valor.";
                } else if (error.email) {
                    return "Por favor, introduzca un Email v\u00e1lido";
                } else if (error.pattern) {
                    return "Por favor, introduzca un valor v\u00e1lido";
                } else if (error.minlength) {
                    return "El password debe ser mayor a 8 caracteres";
                }
            }
        }
    });
    app.controller("RecoverAccountController", function ($scope, $routeParams, $http, LoginRepository) {

        $scope.originalUser = angular.copy($scope.user);
        var route = $scope.$root.server + "company/account/recovery-token";

        $scope.resetUserForm = function (isOk) {
            $scope.successfull = isOk;
            $scope.showMessage = true;
            $scope.user = angular.copy($scope.originalUser);
            $scope.userForm.$setPristine();
            $scope.submitProgress = false;
            $scope.showValidation = false;
        };

        var _onRecover = function (data) {
            $scope.resetUserForm(true);
        };

        var _onRecoverError = function (error) {
            $scope.resetUserForm(false);
        };

        $scope.recoverAccount = function (userDetails) {
            if (!$scope.userForm.$invalid) {
                var recover = {
                     "token": $routeParams.token,
                    "new_password": userDetails.password,
                    "password_confirmation": userDetails.confirmPassword
                }
                $scope.submitProgress = true;
                LoginRepository.recoverAccount({}, recover, _onRecover, _onRecoverError);
            } else {
                $scope.showValidation = true;
            }
        };

        $scope.getError = function (error) {
            if (angular.isDefined(error)) {
                if (error.required) {
                    return "Por favor, introduzca un valor.";
                } else if (error.email) {
                    return "Por favor, introduzca un Email v\u00e1lido";
                } else if (error.pattern) {
                    return "Por favor, introduzca un valor v\u00e1lido";
                } else if (error.minlength) {
                    return "El password debe ser mayor a 8 caracteres";
                }
            }
        }
    });
    app.config(function ($routeProvider) {
        $routeProvider
            .when("/login", {
                templateUrl: "login/partials/index.html",
                css: "login/login.css",
                controller: "LoginController"
            }).when("/login/recover", {
                templateUrl: "login/partials/recover.html",
                css: "login/login.css",
                controller: "RecoverController"
            }).when("/login/recoveraccount/:token", {
                templateUrl: "login/partials/recoveraccount.html",
                css: "login/login.css",
                controller: "RecoverAccountController"
            }).when("/login/activate/:token",{
                templateUrl: "login/partials/index.html",
                css: "login/login.css",
                controller: "LoginController"
            });
    });
})();