﻿'use strict';
var app = angular.module("tattlerApp");
app.factory('authService', ['$http', '$q', 'localStorageService', 'AppConfig', '$rootScope', '$location', 'CompanyRepository', '$cookieStore', function ($http, $q, localStorageService, AppConfig, $rootScope, $location, CompanyRepository, $cookieStore) {

    var serviceBase = AppConfig.server;
    var clientId = AppConfig.clientId;
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: "",
        useRefreshTokens: false
    };

    var _externalAuthData = {
        provider: "",
        userName: "",
        externalAccessToken: ""
    };

    var regexPathArray = ["^\/dashboard$",
        "^\/promotions$", "^\/promotions/new$", "^\/promotions\/.*?\/edit$",
        "^\/wizard$",
        "^\/contacts$",
        "^\/adwords$", "^\/adwords/new$",
        "^\/branches$", "^\/branches\/statistics$", "^\/branches\/logs$", "^\/branches\/list$","^\/branches\/.*?$",
        "^\/company\/profile\/edit$"
    ];

    var publicRoutes = ["/register", "/login/recover",
        "/login/recoveraccount/:token",
        "/login/activate/:token"];

    var inValidPath = function (needle, haystack) {
        var key = '';
        for (key in haystack) {
             var resultArray = needle.match(haystack[key]);
             if (resultArray != null && resultArray.length == 1)
             return true

        }
        return false;
    };

    angular.isUndefinedOrNull = function(val) {
        return angular.isUndefined(val) || val === null;
    };

    var in_array = function (needle, haystack) {

        var key = '';
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }

        return false;
    };     

    var _saveRegistration = function (registration) {

        _logOut();

        return $http.post(serviceBase + 'account/register', registration).then(function (response) {
            return response;
        });

    };

    var _login = function (loginData) {

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        if (loginData.useRefreshTokens) {
            data = data + "&client_id=" + clientId;
        }

        var deferred = $q.defer();

        $http.post(serviceBase + 'accounts/login', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

            if (loginData.useRefreshTokens) {
                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: response.refresh_token, useRefreshTokens: true });
            }
            else {
                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: "", useRefreshTokens: false });
            }
            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;
            _authentication.useRefreshTokens = loginData.useRefreshTokens;

            $rootScope.showMenu = true;
            $rootScope.username = loginData.userName;
            $location.path('/dashboard');

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();

            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        _authentication.isAuth = false;
        _authentication.userName = "";
        _authentication.useRefreshTokens = false;

        $rootScope.showMenu = false;                    
        $location.path('/login');

    };

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
            _authentication.useRefreshTokens = authData.useRefreshTokens;
        }

    };

    var _refreshToken = function () {
        var deferred = $q.defer();

        var authData = localStorageService.get('authorizationData');

        if (authData) {

            if (authData.useRefreshTokens) {

                var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + clientId;

                localStorageService.remove('authorizationData');

                $http.post(serviceBase + 'accounts/login', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                    localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true });

                    deferred.resolve(response);

                }).error(function (err, status) {
                    _logOut();
                    deferred.reject(err);
                });
            }
        }

        return deferred.promise;
    };

    var _obtainAccessToken = function (externalData) {

        var deferred = $q.defer();

        $http.get(serviceBase + 'api/account/ObtainLocalAccessToken', { params: { provider: externalData.provider, externalAccessToken: externalData.externalAccessToken } }).success(function (response) {

            localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });

            _authentication.isAuth = true;
            _authentication.userName = response.userName;
            _authentication.useRefreshTokens = false;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _registerExternal = function (registerExternalData) {

        var deferred = $q.defer();

        $http.post(serviceBase + 'api/account/registerexternal', registerExternalData).success(function (response) {

            localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });

            _authentication.isAuth = true;
            _authentication.userName = response.userName;
            _authentication.useRefreshTokens = false;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _checkStatus = function () {
        var authData = localStorageService.get('authorizationData');

        var location = $location.path();

        if (inValidPath(location, regexPathArray) && (angular.isUndefinedOrNull(authData))) {
            $rootScope.showMenu = false;
            $location.path("/login");
        }
        /*en el caso de que intente acceder al login y ya haya iniciado sesión lo mandamos a la home*/
        else if (location == "/login" && (!angular.isUndefinedOrNull(authData))) {
            $rootScope.showMenu = true;
            $rootScope.username = authData.userName;                   
            $location.path("/dashboard"); 
        }
        /*en el caso de que intente acceder a la ruta privada y ya haya iniciado sesión lo mandamos a la home*/
        else if (inValidPath(location, regexPathArray) && (!angular.isUndefinedOrNull(authData))) {
            $rootScope.username = authData.userName;
            $rootScope.showMenu = true;

            CompanyRepository.current({}, function (company) {

                /*Si usuario no ha llenado su perfil, redirigirlo al wizard inicial
                if (company.description == null && $location.path() != "/wizard") {
                    $location.path("/wizard");
                    return;
                }*/

                $rootScope.profileImage = company.profile_image;

                switch (location) {
                    case "/wizard":
                        $rootScope.showMenu = false;
                        $rootScope.titulo = "Wizard";
                        $rootScope.onWizard = true;
                        break;
                    case "/dashboard":
                        $rootScope.activated = 1;
                        $rootScope.titulo = "Panel Administrativo";
                        $rootScope.icono = "fa fa-dashboard";
                        break;
                    case "/adwords/new":
                    case "/promotions":
                    case "/promotions/new":
                        $rootScope.activated = 2;
                        $rootScope.titulo = "Promociones";
                        $rootScope.icono = "glyphicon glyphicon-gift";
                        break;
                    case "/branches/statistics":
                    case "/branches/logs":
                    case "/branches/list":
                    case "/branches":
                        $rootScope.activated = 3;
                        $rootScope.titulo = "Sucursales";
                        $rootScope.icono = "fa fa-th";
                        break;
                    case "/contacts":
                        $rootScope.activated = 4;
                        $rootScope.titulo = "Contactos";
                        $rootScope.icono = "glyphicon glyphicon-user";
                        break;
                    case "/company/profile/edit":
                        $rootScope.activated = 5;
                        $rootScope.titulo = "Perfil Empresa";
                        $rootScope.icono = "fa-bar-chart-o";
                        break;
                }

            }, function (error) { /*No se pudo verificar el estado de la empresa, seguramente venció el authToken así que se cierra sesión*/
                _logOut();
            });

        } else if (in_array(location, publicRoutes)) {
            $rootScope.showMenu = false;
        }
    };

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.refreshToken = _refreshToken;

    authServiceFactory.obtainAccessToken = _obtainAccessToken;
    authServiceFactory.externalAuthData = _externalAuthData;
    authServiceFactory.registerExternal = _registerExternal;

    authServiceFactory.checkStatus = _checkStatus;

    return authServiceFactory;
}]);