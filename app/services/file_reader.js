(function () {

    'use strict';

    var app = angular.module("tattlerApp");

    app.factory('FileUtils', ['$timeout', function ($timeout) {

        //Constants
        var fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);

        var _generateThumb = function (model, files) {
            if (!fileReaderSupported)
                return null;

            if (files === null || files === "")
                return null;

            var file = files[0];

            /*
            if(typeof(file) != 'Blob'){
                return null;
            }
            */

            $timeout(function () {
                var fileReader = new FileReader();
                fileReader.readAsDataURL(file);
                fileReader.onload = function (e) {
                    $timeout(function () {
                        model.dataUrl = e.target.result;
                    });
                }
            });
        };

        /**
         * Converts data uri to Blob. Necessary for uploading.
         * @see
         *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
         * @param  {String} dataURI
         * @return {{blob: Blob, filename: string}}
         */
        var _dataURItoBlob = function(dataURI) {

            var binary = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var array = [];
            for(var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }

            var blob = new Blob([new Uint8Array(array)], {type: mimeString});
            var filename =  'image.' + mimeString.split('/')[1];

            return {
                blob : blob,
                filename: filename
            };


        };

        return {
            generateThumb: _generateThumb,
            dataURItoBlob: _dataURItoBlob
        }

    }]);

})();